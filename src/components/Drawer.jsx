import * as React from 'react'
import Box from '@mui/material/Box'
import Drawer from '@mui/material/Drawer'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import InboxIcon from '@mui/icons-material/MoveToInbox'
import MailIcon from '@mui/icons-material/Mail'
import { makeStyles } from '@mui/styles'
import { Button, Theme, Toolbar, Paper } from '@mui/material'
import config from '../config'



const useStyles = makeStyles((theme) => ({
    root: {
        backgroundColor: console.log(theme)
    },
    paper: {
        backgroundColor: theme.palette.primary.main
    }
}))

const DrawerNav = ({ open, toggle }) => {

    const classes = useStyles()

    return (
        <Box sx={{ display: 'flex' }}>
            <Drawer
                anchor='left'
                open={open}
                onClose={toggle}
                variant="persistent"
                sx={{
                    width: config.drawerWidth,
                    flexShrink: 0,
                    [`& .MuiDrawer-paper`]: { width: config.drawerWidth, boxSizing: 'border-box', bgcolor: 'primary.main', border: 'none' },
                }}
            >
                <Toolbar />
                <List>
                    {['Inbox', 'Starred', 'Send email', 'Drafts'].map((text, index) => (
                        <ListItem button key={text}>
                            {/* <ListItemIcon>
                                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                            </ListItemIcon> */}
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List>
                <List>
                    {['All mail', 'Trash', 'Spam'].map((text, index) => (
                        <ListItem button key={text}>
                            {/* <ListItemIcon>
                                {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
                            </ListItemIcon> */}
                            <ListItemText primary={text} />
                        </ListItem>
                    ))}
                </List>
            </Drawer>
        </Box>
    );
}

export default DrawerNav
