import React from 'react'
import { DefaultTheme, makeStyles } from '@mui/styles'
import { Button, Theme, Typography, Paper } from '@mui/material'
import { lightBlue } from '@mui/material/colors'


const useStyles = makeStyles((theme: Theme) => ({
    root: {
        height: '100vh'
    },
    loginBox: {
        display: 'flex',
        border: `1px solid ${theme.palette.grey[400]}`,
        width: '60%',
        position: 'relative',
        top: '20%',
        left: '20%',
        height: '600px'
    },
    left: {
        backgroundColor: '#fafa',
        width: '50%'
    },
    right: {
        backgroundColor: `${theme}`,
        width: '50%'
    }
}))

interface loginProps {
    username?: string
    password?: string
}

export const Login: React.FC = (props) => {
    const classes = useStyles()
    return (
        <Paper className={classes.root} square elevation={0}>
            <div className={classes.loginBox} >
                <div className={classes.left}>
                    Login
                </div>
                <div className={classes.right}>
                    <Typography>
                        Hello ! Welcome back
                    </Typography>

                    <Button color="primary" variant="contained">Login</Button>

                </div>
            </div>

        </Paper>
    )
}