import React, { PureComponent } from 'react'
import Header from '../../components/Header'
import { Box, Toolbar, Typography } from '@mui/material'
import { withStyles } from '@mui/styles'
import BasicForm from '../../Forms/BasicForm'
import config from '../../config'

const styles = theme => ({
    root: {
        backgroundColor: theme.palette.primary.main,
        height: '40vh'
    },
    typo: {
        color: theme.palette.primary.contrastText
    }
})

class Landing extends PureComponent {
    render() {
        const { classes } = this.props;
        return (
            <>
                <Header />
                <Box component="main" sx={{ flexGrow: 1, p: 2, width: true ? `calc(100% - ${config.drawerWidth}px)` : '100%', ml: true ? `${config.drawerWidth}px` : '0px' }} className={classes.root}>
                    <Toolbar />
                    <Box display="flex" justifyContent="space-between">
                        <Box>
                            <Typography variant="h5" paddingY={1} className={classes.typo}>
                                Basic Form
                            </Typography>

                        </Box>
                        <Box className={classes.typo}>
                            Bread crum
                        </Box>
                    </Box>
                    <BasicForm action={''} />
                </Box>
            </>
        )
    }
}

export default withStyles(styles)(Landing)