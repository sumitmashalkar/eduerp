import * as React from "react";
import { Grid, TextField, Paper, Typography, Button } from '@mui/material'


class BasicForm extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {

        return (
            <>
                <Paper sx={{ p: 4, borderRadius: 4 }} elevation={2}>
                    <form onSubmit={() => { }} noValidate={true}>
                        <Grid container justifyContent="space-evenly" direction="row" spacing={4}>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="first name" fullWidth placeholder="Enter your first name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="last name" fullWidth placeholder="Enter your last name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="first name" fullWidth placeholder="Enter your first name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="last name" fullWidth placeholder="Enter your last name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="first name" fullWidth placeholder="Enter your first name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} sm={6} md={4}>
                                <TextField label="last name" fullWidth placeholder="Enter your last name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12}>
                                <TextField label="message" multiline rows={2} fullWidth placeholder="Enter your last name" size="small" color="primary"></TextField>
                            </Grid>
                            <Grid item xs={12} container justifyContent="flex-end">
                                <Button variant="contained">Submit</Button>
                            </Grid>
                        </Grid>
                    </form>
                </Paper>
            </>
        );
    }
}

export default BasicForm